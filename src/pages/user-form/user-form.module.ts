import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserFormPage } from './user-form';
import { AngularFireAuthModule } from 'angularfire2/auth';

@NgModule({
  declarations: [
    UserFormPage,
  ],
  imports: [
    IonicPageModule.forChild(UserFormPage),
    AngularFireAuthModule
  ],
})
export class UserFormPageModule {}
