import { User } from './../../models/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from "angularfire2/auth";

@IonicPage()
@Component({
  selector: 'page-user-form',
  templateUrl: 'user-form.html',
})
export class UserFormPage {

  public user = {} as User;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AngularFireAuth) {
  }

  async newUser(user: User) {
    try {
      const result = await this.auth.auth.createUserWithEmailAndPassword(user.email, user.password);
      console.log(result);

      if (result && result.email & result.uid) {
        this.navCtrl.setRoot('HomePage')
      }

    } catch (error) {
      console.error(error);
    }
  }

}
