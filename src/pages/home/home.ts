import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  public form: any = {
    outgo: '',
    title: ''
  }
  private user: any
  public items: Observable<any[]>

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AngularFireAuth,
    private database: AngularFireDatabase,
    private toastCtrl: ToastController
  ) {
  }

  ionViewDidLoad() {
    this.auth.authState.subscribe(res => {
      if (res) {
        this.user = res
        this.loadOutgo()
      } else {
        this.toastCtrl.create({
          message: 'É necessário fazer login para acessar.',
          duration: 4000
        }).present()
        this.navCtrl.setRoot('LoginPage')
      }
    })
  }

  loadOutgo() {
    this.items = this.database.list('outgo',
      ref => ref.orderByChild('uid').equalTo(this.user.uid)
    ).valueChanges()
  }

  registerOutgo(val: string) {
    if (val) {
      val = val.replace(/\D/g, "") //Remove tudo o que não é dígito
      val = val.replace(/(\d)(\d{2})$/, "$1,$2") //Coloca virgula antes dos 2 últimos numeros
      val = val.length === 2 ? '00,' + val : val
      this.form.outgo = val

      this.database.list('outgo')
      .push({ 'uid': this.user.uid, 'outgo': this.form.outgo, 'title': this.form.title, 'createdAt': new Date().toLocaleDateString() })
      .then(res => {
        console.log('dados guardados: ' + res);
      }, e => {
        console.error(e);
      })
    } else {
      this.toastCtrl.create({ message: 'É necessário informar um valor.', duration: 4000 })
      .present()
    }
  }

  formatOutgo(e) {
    let val: string = e.target.value
    val = val.replace(/\D/g, "") //Remove tudo o que não é dígito
    val = val.replace(/(\d)(\d{2})$/, "$1,$2") //Coloca virgula antes dos 2 últimos numeros
    if (e.type === 'blur') {
      val = val.length === 2 ? '00,' + val : val
      this.form.outgo = val
    }
    e.target.value = val
  }

}
