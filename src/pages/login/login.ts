import { User } from './../../models/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AngularFireAuth } from "angularfire2/auth";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public user = {} as User

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AngularFireAuth,
    public toast: ToastController,
    public loadingCtrl: LoadingController
  ) {
    let loading = this.loadingCtrl.create({ content: 'Aguarde...' })
    loading.present()

    this.auth.authState.subscribe(res => {
      loading.dismiss()

      if (res && res.email && res.uid) {
        this.navCtrl.setRoot('HomePage')
      }

    })
  }

  async login(user: User) {
    try {
      const result = await this.auth.auth.signInWithEmailAndPassword(user.email, user.password)
      
      if (result && result.uid) {
        this.navCtrl.setRoot('HomePage')
      } else {
        this.toast.create({
          message: 'Email ou senha incorretos. Por favor, tente novamente.',
          duration: 4000
        }).present()
      }

    } catch (error) {
      console.error(error)
      this.toast.create({
        message: 'Houve um problema inesperado. Por favor, tente novamente.',
        duration: 4000
      }).present()
    }
  }

  newUser() {
    this.navCtrl.push('UserFormPage')
  }
}
